import unittest
from unittest.mock import patch
from tests.test_scraping.fixtures import *
from bootcamp_cri.scraping.scrap_babelio import *


class TestScrapBabelio(unittest.TestCase):

    def test_get_html_from_link(self):
        # Given
        page_link = 'http://www.example.com/'

        # When
        expected_soup = BeautifulSoup(EXEMPLE_HTML, 'html.parser')

        soup = get_html_from_link(page_link)

        # Then
        self.assertEqual(expected_soup, soup)

    def test_extract_book_infos(self):
        # Given
        html = BOOK_HTML
        book_html = BeautifulSoup(html, 'html.parser').find('a')

        # When
        expected_book_links = '/livres/Sachs-Vacances-mortelles-au-paradis/1166729'
        expected_book_title = 'Vacances mortelles au paradis'
        expected_book_image_link = 'https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX95_.jpg'
        expected_tuple = (expected_book_links, expected_book_title, expected_book_image_link)
        book_infos = extract_book_info(book_html)

        # Then
        self.assertEqual(expected_tuple, book_infos)

    def test_extract_author_infos(self):
        # Given
        html = AUTHOR_HTML
        author_html = BeautifulSoup(html, 'html.parser').find('a')

        # When
        expected_author_links = '/auteur/Juliette-Sachs/498755'
        expected_author_name = 'Juliette Sachs'
        expected_tuple = (expected_author_links, expected_author_name)
        author_infos = extract_author_info(author_html)

        # Then
        self.assertEqual(expected_tuple, author_infos)

    @patch('bootcamp_cri.scraping.scrap_babelio.get_html_from_link')
    def test_extract_rate_from_book_page(self, mock_get_html_from_link):
        # Given
        book_link = "/livres/Sachs-Vacances-mortelles-au-paradis/1166729"
        mock_get_html_from_link.return_value = BeautifulSoup(GET_RATE_HTML, 'html.parser')

        # When
        expected_rate = 4.23

        rate = extract_rate_from_book_page(book_link)

        # Then
        self.assertEqual(expected_rate, rate)

    @patch('bootcamp_cri.scraping.scrap_babelio.get_html_from_link')
    def test_get_info_from_page(self, mock_get_html_from_link):
        # Given
        page_link = 'https://www.babelio.com/livrespopulaires_debut.php?p=1'
        mock_get_html_from_link.return_value = BeautifulSoup(GET_INFO_HTML, 'html.parser')
        expected_result = GET_INFO_LIST

        # When
        result = get_info_from_page(page_link)

        # Then
        mock_get_html_from_link.assert_called_once_with(page_link)
        self.assertListEqual(expected_result, result)
